#!/usr/bin/env python3
#
# This file makes use of telnetlib.
#
#
# You should have received a copy of the GNU Lesser General Public License
# along with Paramiko; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.

import getpass
import os
import argparse
import time
import datetime
from multiprocessing.dummy import Pool as ThreadPool
import csv
import telnetlib
import yaml
import random

def ask_pass(user):
    """
    Get user to type in password
    :param str user: Username
    :return: Password (`str`)
    """
    prompt = "Password for {} to SSH to OLT devices: ".format(user)
    password = getpass.getpass(prompt)
    return password


def write_result_to_file(device_type, log_file_name, log_content):
    """
    Append ``log_content_str`` to ``log_file_name`` with timestamp
    :param str device_type: device type to use as log folder name
    :param str log_file_name: name of log file in current working directory
    :param list log_content: log content to write to log file
    :return: ``None``
    """
    pwd = os.getcwd()
    if not os.path.isdir(device_type + '-results'):
        os.makedirs(device_type + '-results')
    item_full_path = os.path.join(pwd, device_type + '-results/' + log_file_name + '-result.yml')

    with open(item_full_path, 'w') as outfile:
        yaml.dump(log_content, outfile, default_flow_style=False)
    return


class TelnetCiscoViaAten(object):
    """
    A session to a Cisco device (MGMT/MGMTSW) console port via telnet to ATEN console server port 5000+
    Main purpose is to check console port status of a Cisco device, including connectivity and authentication method
    """
    def __init__ (self,aten, port, username, password):
        """
        Create a new session
        :param str aten: Name of the ATEN console server for the Cisco device
        :param int port: Serial port number on `aten` for the Cisco device
        :param str username: Username to log in to ATEN console server via telnet port 5000+
        :param str password: Password to log in to ATEN console server via telnet port 5000+
        """
        self.aten = aten
        self.telnet_port = 5000 + port
        self.username = username
        self.password = password
        self.port = port
        """
        Result of the serial port check in a dictionary with the following keys/values
        serial_status
            'working': the serial port connectivity is in place
            'could-not-check': the serial port connectivity couldn't be checked
        device_subtype
            'cisco': the device connecting to the serial port is a Cisco device
            'unknown': the device type of the device connecting to the serial port is unknown
        serial_substatus
            'no-cs-login-prompt': No login prompt is seen when telnet to ATEN console server
            'no-cs-password-prompt': No password prompt is seen when telnet to ATEN console server
            'serial-port-busy': Serial port is being used by a different session 
            'cs-rad-auth-failed': Authentication to ATEN console server using RADIUS account fails
            'working-with-rad-auth': Authentication to the device connecting to the serial port works with RADIUS 
            'working-with-local-account': Authentication to the device connecting to the serial port works with local account
        """
        self.summary_result = {}
        # prefix of the log file to store ATEN console server logs during script run
        self.log_file_name = aten
        # Run check() function
        try:
            self.check()
        except:
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'unknown'}
            pass

    def log(self, log_content_str):
        """
        Append ``log_content`` to ``log_file_name`` with timestamp
        :param str log_content_str: log content to write to log file
        :return: ``None``
        """
        pwd = os.getcwd()
        if not os.path.isdir('cons' + '-logs'):
            os.makedirs('cons' + '-logs')
        item_full_path = os.path.join(pwd, 'cons' + '-logs/' + self.log_file_name + '.log')

        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

        INDENT = " "

        log_content_list = log_content_str.split('\r\n')

        with open(item_full_path, 'a') as a_file:
            a_file.write('\n[' + st + ']\n')
            for line in log_content_list:
                a_file.write(INDENT * 25 + line + '\n')

    def check(self):
        """
        Main function to check serial port status. Main logic is as below
        1. Telnet to ATEN console server port port 5000+ using RADIUS account
            Stop if
                Telnet connection is refused
                Telnet connections times out
                Expected login/password prompt is not seen
                Serial port is busy
        2. Log in to end device of the serial port using RADIUS account
        3. Log in to end device of the serial port using local account
        :return:
        """
        # Telnet to ATEN serial port
        try:
            tn = telnetlib.Telnet(self.aten, self.telnet_port, timeout=8)
        except:
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'unknown'}
            return

        # Log in with RADIUS account, should see 'Suspend Menu.' if successful
        tn_read = tn.read_until(b"Login:", 4)
        print(tn_read)
        self.log(tn_read.decode('utf-8'))

        # Login prompt is not seen
        if "Login:" not in tn_read.decode('utf-8'):
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'no-cs-login-prompt'}
            return

        tn.write(self.username.encode('ascii') + b"\r")
        tn_read = tn.read_until(b"Password:", 4)
        print(tn_read)
        self.log(tn_read.decode('utf-8'))

        # Password prompt is not seen
        if "Password:" not in tn_read.decode('utf-8'):
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'no-cs-password-prompt'}
            return

        tn.write(self.password.encode('ascii') + b"\r")
        tn_read = tn.read_until('Suspend Menu.'.encode('ascii'), 9)
        print(tn_read)
        self.log(tn_read.decode('utf-8'))

        # If see 'busy', serial port is busy
        if "busy" in tn_read.decode('utf-8'):
            self.log("Serial port is busy")
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'serial-port-busy'}

            # Press '\r'
            tn.write(b"\r")
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
            return

        # If don't see 'Suspend Menu.', RADIUS authentication doesn't work, need manual fallout
        if "Suspend Menu." not in tn_read.decode('utf-8'):
            self.log("RADIUS authentication doesn't work, need manual fallout")
            self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': 'cs-rad-auth-failed'}

            # Press 'CTRL-C'
            tn.write(chr(3).encode('ascii'))
            tn.close()
            return

        # Hit enter, should see 'Username'
        tn.write(b"\r")
        y = tn.read_until(b'Username: ', 10)
        print(y)
        self.log(y.decode('utf-8'))

        # Happy path
        if "Username: " in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            print("Good Cisco prompt")
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Good Cisco prompt")
            tn.write(self.username.encode('ascii') + b"\r")
            tn_read = tn.read_until(b"Password: ", 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            tn.write(self.password.encode('ascii') + b"\r")
            tn_read = tn.read_until('>'.encode('ascii'), 10)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Happy path, see "#" for devices with "aaa authorization console" applied, or ">" if not
            if "#" in tn_read.decode('utf-8') or ">" in tn_read.decode('utf-8'):
                print("Cisco, Good RADIUS")
                self.log("Cisco, Good RADIUS")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'cisco',
                                       'serial_substatus': 'working-with-rad-auth'}
                # Press 'exit'
                tn.write("exit".encode('ascii') + b"\r\r")
                tn_read = tn.read_until(b'Username: ', 3)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

            # RADIUS account doesn't work, try local account
            elif "Unauhorized for netgear" in tn_read.decode('utf-8'):
                print("RADIUS account doesn't work, trying local account")
                self.log("RADIUS account doesn't work, trying local account")

                tn.write('cfadmin1'.encode('ascii') + b"\r")
                tn_read = tn.read_until(b"Password: ", 4)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                tn.write("2-15-B3df0rd!".encode('ascii') + b"\r")
                tn_read = tn.read_until('#'.encode('ascii'), 15)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                # Local account works
                if "#" in tn_read.decode('utf-8') or ">" in tn_read.decode('utf-8'):
                    print("Cisco, local account")
                    self.log("Cisco, local account")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'cisco',
                                           'serial_substatus': 'working-with-local-account'}
                    # Press 'exit'
                    tn.write("exit".encode('ascii') + b"\r\r")
                    tn_read = tn.read_until(b'Username: ', 3)
                # Local account doesn't work
                else:
                    print("Manual fallout")
                    self.log("Manual fallout")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                           'serial_substatus': 'both-rad-and-local-account-not-working'}
            # Unexpected prompt, manual fallout is required
            else:
                print("Not sure what this is, need to check further")
                self.log("Not sure what this is, need to check further")
                self.summary_result = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                       'serial_substatus': 'require-further-checking'}

            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
            return

        # A working session someone forgot to log out
        elif "#" in y.decode('utf-8') or ">" in y.decode('utf-8'):
            print("blah")
            print("Initial prompt of the device is {}".format(y.decode('utf-8')))
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8')))

            # Log out by hitting 'CTRL-Z' then exit
            # Press 'CTRL-Z'
            tn.write(chr(26).encode('ascii'))

            # Press 'exit'
            tn.write("exit".encode('ascii') + b"\r\r")
            tn_read = tn.read_until(b'Username: ', 4)
            print(tn_read)

            # Type in RADIUS account
            tn.write(self.username.encode('ascii') + b"\r")
            tn_read = tn.read_until(b"Password: ", 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            tn.write(self.password.encode('ascii') + b"\r")
            tn_read = tn.read_until('>'.encode('ascii'), 10)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # RADIUS account works
            if "#" in y.decode('utf-8') or ">" in y.decode('utf-8'):
                print("Cisco, Good RADIUS")
                self.log("Cisco, Good RADIUS")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'cisco',
                                       'serial_substatus': 'working-with-rad-auth'}
                # Press 'exit'
                tn.write("exit".encode('ascii') + b"\r\r")
                tn_read = tn.read_until(b'Username: ', 3)
            # RADIUS account doesn't work, try local account
            elif "Unauhorized for netgear" in tn_read.decode('utf-8'):
                print("RADIUS account doesn't work, trying local account")
                self.log("RADIUS account doesn't work, trying local account")

                tn.write('cfadmin1'.encode('ascii') + b"\r")
                tn_read = tn.read_until(b"Password: ", 4)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                tn.write("2-15-B3df0rd!".encode('ascii') + b"\r")
                tn_read = tn.read_until('>'.encode('ascii'), 15)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                # Local account works
                if "#" in tn_read.decode('utf-8') or ">" in tn_read.decode('utf-8'):
                    print("Cisco, local account")
                    self.log("Cisco, local account")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'cisco',
                                           'serial_substatus': 'working-with-local-account'}
                    # Press 'exit'
                    tn.write("exit".encode('ascii') + b"\r\r")
                    tn_read = tn.read_until(b'Username: ', 3)
                # Local account doesn't work
                else:
                    print("Manual fallout")
                    self.log("Manual fallout")
                    self.summary_result = {'serial_status': 'unsure', 'device_subtype': 'unknown',
                                           'serial_substatus': 'both-rad-and-local-account-not-working'}
            # Unexpected prompt, manual fallout
            else:
                print("Not sure what this is, need to check further")
                self.log("Not sure what this is, need to check further")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                       'serial_substatus': 'unknown'}

            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
        # Someone left a session after typing in 'Username'
        elif "Password: " in y.decode('utf-8'):
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))

            # Reset the session by hitting 'CTRL-C' then enter
            # Press CTRL-C
            tn.write(chr(3).encode('ascii'))
            tn_read = tn.read_until(b"Press RETURN to get started.", 2)

            # Press enter, expect to see 'Username: '
            tn.write(b"\r")
            tn_read = tn.read_until(b"Username: ", 3)

            # Happy path, try RADIUS account
            if "Username: " in tn_read.decode('utf-8'):
                print("Calix E9-2, active or standby")
                self.log("Calix E9-2, active or standby")

                tn.write(self.username.encode('ascii') + b"\r")
                tn_read = tn.read_until(b"Password: ", 4)

                tn.write(self.password.encode('ascii') + b"\r")
                tn_read = tn.read_until('>'.encode('ascii'), 30)

                # RADIUS account works
                if "#" in tn_read.decode('utf-8'):
                    print("Calix OLT E9-2, active card")
                    self.log("Calix OLT E9-2, active card")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                           'serial_substatus': 'active-card'}
                    # Press 'exit'
                    tn.write("exit".encode('ascii') + b"\r")
                    tn_read = tn.read_until(b'login: ', 4)
                # RADIUS account doesn't work
                elif "Unauhorized for netgear" in tn_read.decode('utf-8'):
                    print("Calix OLT E9-2, standby card")
                    self.log("Calix OLT E9-2, standby card")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'calix-e9',
                                           'serial_substatus': 'standby-card'}
                    # Press CTRL-C
                    tn.write(chr(3).encode('ascii'))
                    tn_read = tn.read_until(b"login: ", 1)
                # Unexpected prompt, manual fallout
                else:
                    print("Not sure what this is, need to check further")
                    self.log("Not sure what this is, need to check further")
                    self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                           'serial_substatus': 'unknown'}

                # Press 'CTRL-D'
                tn.write(chr(4).encode('ascii'))
                tn_read = tn.read_until(b'one: ', 4)
                print(tn_read)
                self.log(tn_read.decode('utf-8'))

                # Press 'q'
                tn.write("q".encode('ascii'))

                tn.close()
                return
            # Unexpected prompt, manual fallout
            else:
                print("Not sure what this is, need to check further")
                self.log("Not sure what this is, need to check further")
                self.summary_result = {'serial_status': 'working', 'device_subtype': 'unknown',
                                       'serial_substatus': 'unknown2'}
        # Unexpected prompt, manual fallout
        else:
            print("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            print("Manual fallout or fail console port")
            self.log("Initial prompt of the device is {}".format(y.decode('utf-8').splitlines()))
            self.log("Manual fallout or fail console port")
            self.summary_result = {'serial_status': 'unknown', 'device_subtype': 'unknown',
                                   'serial_substatus': 'unknown'}
            # Press 'CTRL-D'
            tn.write(chr(4).encode('ascii'))
            tn_read = tn.read_until(b'one: ', 4)
            print(tn_read)
            self.log(tn_read.decode('utf-8'))

            # Press 'q'
            tn.write("q".encode('ascii'))

            tn.close()
            return


class TelnetAten(object):
    """
    A telnet session to ATEN console server to retrieve serial port list, and to initiate a serial port status check
    """
    def __init__(self, aten, username, password):
        self.serial_port_list = []
        self.aten = aten
        self.username = username
        self.password = password
        self.exception = ""
        try:
            self.retrieve_serial_port_list()
        except Exception as e:
            self.exception = str(e)
            print(self.exception)
            pass

    def retrieve_serial_port_list(self):
        try:
            tn = telnetlib.Telnet(self.aten, port=23, timeout=8)
        except Exception as e:
            self.exception = str(e)
            print(self.exception)
            return

        # Enter username and password to log in to ATEN
        tn.read_until(b"Login:", 4)

        tn.write(self.username.encode('ascii') + b"\r")
        tn.read_until(b"Password:", 4)
        tn.write(self.password.encode('ascii') + b"\r")
        read_output = tn.read_until(b'one:', 4)
        print(read_output.decode('utf-8').split('\r\n'))

        # Check that 'Port Access' is number 3 in ATEN menu
        for line in read_output.decode('utf-8').split('\r\n'):
            if "3.  Port Access" in line.strip():
                break

        # Go to 'Port Access', read serial port list and exit
        tn.write("3".encode('ascii') + b"\r")
        read_output = tn.read_until(b'Q. Exit', 4)
        tn.write("q".encode('ascii'))
        tn.write("q".encode('ascii'))

        cs_serial_port_list = []

        # Remove unused serial ports from serial port name list
        for line in read_output.decode('utf-8').split('\r'):
            cs_serial_ports = line.strip().split(". ")
            if len(cs_serial_ports) > 1:
                if "COM" not in cs_serial_ports[1] and "Exit" not in cs_serial_ports[1]:
                    # cs_serial_port_list.append({cs_serial_ports[1]: cs_serial_ports[0]})
                    cs_serial_port_list.append({'name':cs_serial_ports[1],'port':cs_serial_ports[0]})

        self.serial_port_list = cs_serial_port_list

    def check_a_serial_port_in_aten(self, device_full_dns_name):
        # Retrieve 'abd860-olt-2' from 'abd860-olt-2.network.cityfibre.com'
        device_simple_name = str(device_full_dns_name.split('.')[0])

        # Retrieve 'olt-2' from 'abd860-olt-2'
        device_name_without_site_name = device_simple_name.split('-', 1)[1]

        device_name_without_site_name_removed_dash = device_name_without_site_name.replace("-","")

        # Retrieve 'abd860' from 'abd860-olt-2'
        site_name = device_simple_name.split('-', 1)[0]

        for item in self.serial_port_list:
            # Look in serial ports containing 'olt-2' in lower case
            if device_name_without_site_name in item['name'].lower() or device_name_without_site_name_removed_dash in item['name'].lower():
                tn = TelnetCiscoViaAten(self.aten, int(item['port']), self.username, self.password)
                tn.summary_result['label'] = device_name_without_site_name
                write_result_to_file('cisco-selective-cons', site_name + "-port-" + item['port'], tn.summary_result)


def check_a_serial_port_in_aten(console_variable_list):
    """
    Check a serial port status - in a function format to fit concurrent thread model
    :param console_variable_list: a list of the following variables
    1. Cisco device FQDN
    2. username to log in to ATEN
    3. password to log in to ATEN
    :return:
    """
    # random.randint(1,3)
    time.sleep(0.1 * random.randint(1,20))
    device_name = console_variable_list[0]
    username = console_variable_list[1]
    password = console_variable_list[2]

    # Retrieve 'mgmt-1' from 'mk938-mgmt-1.network.cityfibre.com'
    device_short_name = device_name.split('.')[0].split('-', 1)[1]

    # Retrieve 'mk938' from 'mk938-mgmt-1.network.cityfibre.com'
    site_name = device_name.split('.')[0].split('-')[0]

    # Form ATEN console server FQDN 'mk938-cons-1.network.cityfibre.com'
    cs_name = site_name + '-cons-1.network.cityfibre.com'

    try:
        aten_telnet_instance = TelnetAten(cs_name, username, password)
        if aten_telnet_instance.serial_port_list:
            aten_telnet_instance.check_a_serial_port_in_aten(device_name)
        else:
            x = {'serial_status': 'could-not-check', 'device_subtype': 'unknown',
                                   'serial_substatus': aten_telnet_instance.exception, 'label':device_short_name}
            write_result_to_file('cisco-selective-cons', site_name + "-port-" + "unknown" + device_short_name, x)
    except Exception as e:
        print("*** Caught exception: {}: {}".format(e.__class__, e))
        # pass


def main():
    """
    Collect inventory data from seed file and run `cisco_task` to devices in parallel
    :return: None
    """

    # Number of parallel tasks
    concurrent_thread_number = 20

    # Load seed file to 'inventory'
    parser = argparse.ArgumentParser(description='Read seed file')
    parser.add_argument('--seed', required=True)
    args = parser.parse_args()
    f = open(args.seed, "r")
    inventory = yaml.load(f.read(), Loader=yaml.FullLoader)

    # Load credentials file
    credential_file = open("credentials.yml", "r")
    credentials = yaml.load(credential_file.read(), Loader=yaml.FullLoader)

    # Set username and password for Telnet sessions
    username = credentials["credentials"]["username"]
    password = credentials["credentials"]["password"]
    # password = ask_pass(username)

    # Create variable list for thread task
    console_task_variable_list = []
    for item in inventory["cisco-887"]:
        console_task_variable_list.append([item, username, password])
    # for item in inventory["cisco-2960"]:
    #     console_task_variable_list.append([item, username, password])

    # Run multiple tasks as threads in parallel
    pool = ThreadPool(concurrent_thread_number)
    pool.map(check_a_serial_port_in_aten, console_task_variable_list)

    # Write results to file in csv
    with open('CHG logs/cisco-selective-cons-result.csv', 'w', newline='') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        # Get a sorted list of Cisco device serial port status result files
        file_list = os.listdir("./cisco-selective-cons-results")
        file_list.sort()

        # Write the first line in csv file with column headers
        wr.writerow(['site_name','console_server_port','device_name','device_type','device_serial_status','device_serial_substatus'])
        for filename in file_list:
            site_name = filename.split('-')[0]
            port = filename.split('-')[2]
            with open("./cisco-selective-cons-results/" + filename, 'r') as a_file:
                x = yaml.load(a_file.read(), Loader=yaml.FullLoader)
                wr.writerow([site_name, port, x['label'], x['device_subtype'], x['serial_status'], x['serial_substatus']])


if __name__ == '__main__':
    main()
