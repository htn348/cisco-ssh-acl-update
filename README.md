Introduction
------------
This repository makes use of [paramiko](https://github.com/paramiko/paramiko) and telnetlib to provide a script to update SSH ACL on Cisco 887 and Cisco 2960L devices in CFH Mgmt Network.

The following tasks are run for each Cisco MGMT/MGMTSW device

1. Check console status of Cisco devices, proceed if they are in place
2. PING test to check link quality, proceed if good quality
3. SSH to device
4. Check existing SSH ACL content, proceed if matches existing SSH ACL template
5. Check existing Line VTY config, proceed if matches existing Line VTY config
6. Create CF_SSH_ACCESS-temp with same content as existing SSH ACL template
7. Apply CF_SSH_ACCESS-temp to line vty
8. Delete CF_SSH_ACCESS ACL
9. Create CF_SSH_ACCESS ACL with new content
10. Apply CF_SSH_ACCESS to line vty
11. Save config to NVRAM

Requirement
-----------
* A Linux machine with git, python 3.6 and [virtualenv](https://pypi.org/project/virtualenv/)
* Access to CFH Mgmt Network, Internet and [CityFibre bitbucket](https://bitbucket.org/cityfibre/)

Usage
-----
Clone this repository:

    git clone https://bitbucket.org/cityfibre/cisco-ssh-acl-update

In `cisco-ssh-acl-update` folder, run `virtualenv` and install dependencies

    virtualenv venv
    source venv/bin/activate
    pip install -r dev-requirements.txt
    

In `seedFile.yml`

* Change username to your FreeIPA RADIUS username
* Update the list of Cisco 1921, 887 and 2960L devices to run the script to

Run

    python3 cisco_console_selective_check.py --seed seedFile.yml

Type your FreeIPA password at the prompt.
Console check results are in `cisco-selective-cons-result.csv`. Other logs are in `stdout` or folders `cisco-selective-cons-results` and `cons-logs`


Run
    
    python3 cisco-ssh-acl-update-v3.py --seed seedFile.yml

Type your FreeIPA password at the prompt.

SSH ACL change will then run and produce summarized results in `result.yml` and `result.csv`

Paramiko logs is in `paramiko.log`.

Other SSH ACL change script logs are in `stdout` or in folders

* `cisco-887-logs`
* `cisco-2960-logs`
* `cisco-887-results`
* `cisco-2960-results`